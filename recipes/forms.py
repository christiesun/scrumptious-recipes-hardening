from django import forms
from recipes.models import Rating
from django.shortcuts import redirect


def recipe(respond):
    try:
        from recipes.models import Recipe
    except Recipe.DoesNotExist:
        return redirect("recipes_list")




# except Exception:
    # pass


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]
