from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

from meal_plans.models import Meal_Plans

# Create your views here.


class MealPlansCreateView(LoginRequiredMixin, CreateView):
    model = Meal_Plans
    template_name = "meal_plans/new.html"
    fields = ["name", "date", "recipes"]

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plans_detail", pk=plan.id)


class MealPlansListView(LoginRequiredMixin, ListView):
    model = Meal_Plans
    template_name = "meal_plans/list.html"

    def get_queryself(self):
        return Meal_Plans.objects.filter(owner=self.request.user)


class MealPlansDetailView(LoginRequiredMixin, DetailView):
    model = Meal_Plans
    template_name = "meal_plans/detail.html"

    def get_queryself(self):
        return Meal_Plans.objects.filter(owner=self.request.user)


class MealPlansDeleteView(LoginRequiredMixin, DeleteView):
    model = Meal_Plans
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plan_list")

    def get_queryself(self):
        return Meal_Plans.objects.filter(owner=self.request.user)


class MealPlansUpdateView(LoginRequiredMixin, UpdateView):
    model = Meal_Plans
    template_name = "meal_plans/edit.html"
    fields = ["name", "owner", "date", "recipes"]

    def get_queryself(self):
        return Meal_Plans.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("meal_plans_detail", args=[self.object.id])
