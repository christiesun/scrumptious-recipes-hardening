from django.urls import path

from meal_plans.views import (
    MealPlansCreateView,
    MealPlansDeleteView,
    MealPlansDetailView,
    MealPlansListView,
    MealPlansUpdateView,
)

urlpatterns = [
    path("", MealPlansListView.as_view(), name="meal_plans_list"),
    path("<int:pk>/", MealPlansDetailView.as_view(), name="meal_plans_detail"),
    path("new/", MealPlansCreateView.as_view(), name="meal_plans_new"),
    path("<int:pk>/delete/", MealPlansDeleteView.as_view(), name="_delete"),
    path("<int:pk>/edit/", MealPlansUpdateView.as_view(), name="meal_plans_edit"),
]
